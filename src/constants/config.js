const CONFIG = Object.freeze({
  API_BASE_URL: "https://apihomeez.ddns.net",
  AXIOS_CONFIG: {
    timeout: 15000,
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
  },
});

export default CONFIG;
