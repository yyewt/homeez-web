import CONFIG from "./config";

const API = Object.freeze({
  QUOTATION_CREATE: {
    METHOD: "POST",
    URL: CONFIG.API_BASE_URL + "/quotations",
  },
  QUOTATION_LIST: {
    METHOD: "GET",
    URL: CONFIG.API_BASE_URL + "/quotations",
  },
});

export default API;
