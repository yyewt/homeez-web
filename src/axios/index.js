import axios from "axios";
import CONFIG from "../constants/config";

export default axios.create(CONFIG.AXIOS_CONFIG);
