import React, { Component, createRef } from "react";
import { Link } from "react-router-dom";
import { Formik, Form, Field, ErrorMessage } from "formik";
import * as Yup from "yup";
import "bootstrap/dist/css/bootstrap.min.css";
import API from "../constants/api";
import axiosBase from "../axios";

class QuotationCreate extends Component {
  render() {
    const quotationFormRef = createRef();
    const quotationInitialValues = {
      quotation_info: "",
      quotation_valid: true,
    };

    const quotationSchema = Yup.object().shape({
      quotation_info: Yup.string().required("Quotation Info is required"),
    });

    return (
      <div className="container" style={{ textAlign: "left" }}>
        <div className="row mb-5">
          <div className="col-lg-12 text-center">
            <h1 className="mt-5">Create Quotation</h1>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-12">
            <Formik
              innerRef={quotationFormRef}
              initialValues={quotationInitialValues}
              validationSchema={quotationSchema}
              onSubmit={(values) => {
                quotationFormRef.current.setSubmitting(true);

                axiosBase({
                  method: API.QUOTATION_CREATE.METHOD,
                  url: API.QUOTATION_CREATE.URL,
                  data: values
                })
                  .then((res) => {
                    alert("Form submit successfully");
                    quotationFormRef.current.resetForm(quotationInitialValues);
                    quotationFormRef.current.setSubmitting(false);
                  })
                  .catch((errRes) => {
                    alert("Form submission error. Please retry again.");
                    quotationFormRef.current.setSubmitting(false);
                  });
              }}
            >
              {({ values, touched, errors, isSubmitting, setFieldValue }) => (
                <Form>
                  <div className="form-group">
                    <label htmlFor="quotation_info">Quotation Info</label>
                    <Field
                      as="textarea"
                      name="quotation_info"
                      className={`form-control ${
                        touched.quotation_info && errors.quotation_info
                          ? "is-invalid"
                          : ""
                      }`}
                    />
                    <ErrorMessage
                      component="div"
                      name="quotation_info"
                      className="invalid-feedback"
                    />
                  </div>

                  <div className="form-group">
                    <label htmlFor="quotation_valid">Is Quotation Valid?</label>
                    <div role="group" aria-labelledby="quotation_valid">
                      <label style={{ marginRight: 20 }}>
                        <Field
                          style={{ marginRight: 5 }}
                          type="radio"
                          value={true}
                          name="quotation_valid"
                          checked={values.quotation_valid}
                          onChange={() =>
                            setFieldValue("quotation_valid", true)
                          }
                        />
                        Yes
                      </label>
                      <label>
                        <Field
                          style={{ marginRight: 5 }}
                          type="radio"
                          value={false}
                          name="quotation_valid"
                          checked={!values.quotation_valid}
                          onChange={() =>
                            setFieldValue("quotation_valid", false)
                          }
                        />
                        No
                      </label>
                    </div>
                  </div>

                  <button
                    type="submit"
                    className="btn btn-primary btn-block"
                    disabled={isSubmitting}
                  >
                    {isSubmitting ? "Please wait..." : "Submit"}
                  </button>
                </Form>
              )}
            </Formik>
          </div>
        </div>
        <br />
        <div className="row">
          <div className="col-lg-12" style={{ textAlign: "center" }}>
            <Link to="/quotation/list">Go to Quotation List</Link>
          </div>
        </div>
      </div>
    );
  }
}

export default QuotationCreate;
