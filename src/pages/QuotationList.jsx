import React, { Component } from "react";
import { Link } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";

import API from "../constants/api";
import axiosBase from "../axios";

class QuotationList extends Component {
  state = {
    isLoading: false,
    quotationList: [],
    quotationListError: false,
  };

  renderQuotationListTableBody = () => {
    const MESSAGE_LOADING = "Loading ...";
    const MESSAGE_QUOTATION_LIST_EMPTY = "No record found";
    const MESSAGE_QUOTATION_LIST_ERROR = "Error in fetching Quotation List";

    if (this.state.isLoading) {
      return (
        <tbody>
          <tr>
            <td colSpan="4" style={{ textAlign: "center" }}>
              {MESSAGE_LOADING}
            </td>
          </tr>
        </tbody>
      );
    } else if (this.state.quotationListError) {
      return (
        <tbody>
          <tr>
            <td colSpan="4" style={{ textAlign: "center" }}>
              {MESSAGE_QUOTATION_LIST_ERROR}
            </td>
          </tr>
        </tbody>
      );
    } else if(!this.state.quotationList.length) {
      return (
        <tbody>
          <tr>
            <td colSpan="4" style={{ textAlign: "center" }}>
              {MESSAGE_QUOTATION_LIST_EMPTY}
            </td>
          </tr>
        </tbody>
      );
    } else {
      return (
        <tbody>
          {this.state.quotationList.map((quotation) => (
            <tr key={quotation.q_id}>
              <td>{quotation.q_id}</td>
              <td>{quotation.quotation_info}</td>
              <td>{quotation.quotation_valid ? "Valid" : "Invalid"}</td>
              <td>{new Date(quotation.created_at).toLocaleString()}</td>
            </tr>
          ))}
        </tbody>
      );
    }
  };

  componentDidMount() {
    this.setState({ isLoading: true });
    axiosBase({
      method: API.QUOTATION_LIST.METHOD,
      url: API.QUOTATION_LIST.URL,
    })
      .then((res) => {
        this.setState({ isLoading: false, quotationList: res.data });
      })
      .catch((errRes) => {
        this.setState({ isLoading: false, quotationListError: true });
      });
  }

  render() {
    return (
      <div className="container" style={{ textAlign: "left" }}>
        <div className="row mb-5">
          <div className="col-lg-12 text-center">
            <h1 className="mt-5">Quotation List</h1>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-12">
            <div className="table-responsive">
              <table className="table">
                <thead>
                  <tr>
                    <th>Quotation ID</th>
                    <th>Quotation Info</th>
                    <th>Quotation Valid</th>
                    <th>Quotation Created At</th>
                  </tr>
                </thead>
                {this.renderQuotationListTableBody()}
              </table>
            </div>
          </div>
        </div>
        <br />
        <div className="row">
          <div className="col-lg-12" style={{ textAlign: "center" }}>
            <Link to="/">Go to Create Quotation</Link>
          </div>
        </div>
      </div>
    );
  }
}

export default QuotationList;
