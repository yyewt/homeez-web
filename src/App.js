import logo from "./logo.svg";
import "./App.css";
import { HashRouter, Route } from "react-router-dom";
import QuotationCreate from "./pages/QuotationCreate";
import QuotationList from "./pages/QuotationList";

function App() {
  return (
    <div className="App">
      <HashRouter>
        <Route path="/" exact component={QuotationCreate} />
        <Route path="/quotation/list" component={QuotationList} />
      </HashRouter>
    </div>
  );
}

export default App;