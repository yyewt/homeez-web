# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).


### AWS API Link (Hosted in AWS S3)

[https://homeez-web.s3.us-east-2.amazonaws.com/index.html](https://homeez-web.s3.us-east-2.amazonaws.com/index.html) 

### Runs the app in the development mode

First, `npm install`, follow by `npm start`

Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

